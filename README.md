# Formation R

Répertoire pour trouver les documents de la session 1 de la Formation R - les bases

Vous y trouverez : 

- le Powerpoint : Formation R _ Les bases.pptm 

- le script des exercices avec les solutions 

- un pdf RGuideLinesFr.pdf pour avoir les conventions de codage 

Enjoy 
